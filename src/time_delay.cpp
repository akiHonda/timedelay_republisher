#include "ros/ros.h"
#include <cstdio>
#include "topic_tools/shape_shifter.h"
#include "topic_tools/parse.h"
#include <message_filters/subscriber.h>
#include <message_filters/time_sequencer.h>

#include <string> 

#include <list>

using std::string;
using std::list;
using namespace topic_tools;

ros::NodeHandle *g_node = NULL;
bool g_advertised = false;
string g_input_topic;
string g_output_topic;
ros::Publisher g_pub;
ros::Subscriber* g_sub;

bool g_lazy;
ros::TransportHints g_th;
double g_delay_sec, g_check_rate;

list<ShapeShifter> msg_list;
list<ros::Time> time_list;

void conn_cb(const ros::SingleSubscriberPublisher&);
void in_cb(const ros::MessageEvent<ShapeShifter>& msg_event);

ros::Timer g_timer;

void subscribe()
{
  g_sub = new ros::Subscriber(g_node->subscribe(g_input_topic, 10, &in_cb, g_th));

}

void conn_cb(const ros::SingleSubscriberPublisher&)
{
  // If we're in lazy subscribe mode, and the first subscriber just
  // connected, then subscribe, #3389.
  if(g_lazy && !g_sub)
    {
      ROS_DEBUG("lazy mode; resubscribing");
      subscribe();
    }
}

void timerCallback(const ros::TimerEvent&)
{
  ros::Time current_time = ros::Time::now();
  
  double time_diff = current_time.toSec() - time_list.front().toSec();

  if(time_diff > g_delay_sec)
  {
    if(!msg_list.empty() && !time_list.empty())
      {
	g_pub.publish(msg_list.front());
	//msg_list.pop_front();　// TODO !!
	time_list.pop_front();
      }
  }

  ROS_INFO_STREAM("Timer" << time_diff);

}


void in_cb(const ros::MessageEvent<ShapeShifter>& msg_event)
{
  boost::shared_ptr<ShapeShifter const> const &msg = msg_event.getConstMessage();
  boost::shared_ptr<const ros::M_string> const& connection_header = msg_event.getConnectionHeaderPtr();

  if (!g_advertised)
    {
      // If the input topic is latched, make the output topic latched, #3385.
      bool latch = false;
      if (connection_header)
	{
	  ros::M_string::const_iterator it = connection_header->find("latching");
	  if((it != connection_header->end()) && (it->second == "1"))
	    {
	      ROS_DEBUG("input topic is latched; latching output topic to match");
	      latch = true;
	    }
	}
      g_pub = msg->advertise(*g_node, g_output_topic, 10, latch, conn_cb);
      g_advertised = true;

      
      ROS_INFO("advertised as %s\n", g_output_topic.c_str());
      g_timer = g_node->createTimer(ros::Duration(1.0/g_check_rate), timerCallback);
    }
  // If we're in lazy subscribe mode, and nobody's listening, 
  // then unsubscribe, #3389.
  //if(g_lazy && !g_pub.getNumSubscribers())
  //  {
  //    ROS_DEBUG("lazy mode; unsubscribing");
  //    delete g_sub;
  //    g_sub = NULL;
  //  }
  //else
  //g_pub.publish(msg);

    msg_list.push_back(*msg);
    time_list.push_back(ros::Time::now());
}

int main(int argc, char **argv)
{
  if (argc < 4)
    {
      printf("\nusage: relay IN_TOPIC DELAY_SECOND CHECK_RATE [OUT_TOPIC]\n\n");
      return 1;
    }

  std::string topic_name;
  if(!getBaseName(string(argv[1]), topic_name)) return 1;

  ros::init(argc, argv, topic_name + string("_delay"), ros::init_options::AnonymousName);

  if (argc == 4)
    g_output_topic = string("/delay") + string(argv[1]); 
  else // argc == 4
    g_output_topic = string(argv[4]);

  g_input_topic = string(argv[1]);
  g_delay_sec = strtod(argv[2], NULL);
  g_check_rate = strtod(argv[3], NULL);

  ros::NodeHandle n;
  g_node = &n;
  
  ros::NodeHandle pnh("~");
  bool unreliable = false;
  pnh.getParam("unreliable", unreliable);
  pnh.getParam("lazy", g_lazy);
  if (unreliable)
    g_th.unreliable().reliable(); // Prefers unreliable, but will accept reliable.

  subscribe();
  ros::spin();
  return 0;
}
